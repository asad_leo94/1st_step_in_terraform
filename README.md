# VM Creation
This project will create a virtual machine in Azure. It utilizes Terraform and GitLab CI/CD for a fully automated infrastructure setup.


## Variable Declaration
This project needs different variable for deployment. By default, all the variable are pre-defined exccept in variable.tf. But, It is highly recommended to declare customize variables according to project.Following are the variables;

![Variables declaration](img/Var_VM_declaration.png)

All the resources will be created according to the declare variables. It can be verified through Azure Portal.  

![RG Verfication](img/Resource_Verf.png)
