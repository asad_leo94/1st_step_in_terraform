# Create a resource group
resource "azurerm_resource_group" "main" {
    name     = "${var.RG_NAME}-RG"
    location = var.LOCATION

    tags = {
        environment = "var.environment"
    }
}

# Create virtual network
resource "azurerm_virtual_network" "main" {
    name                = "${var.RG_NAME}-network"
    address_space       = ["10.2.0.0/16"]
    location            = var.LOCATION
    resource_group_name = azurerm_resource_group.main.name

    tags = {
        environment = var.environment
    }
}

# Create subnet
resource "azurerm_subnet" "main" {
    name                 = "${var.RG_NAME}-Subnet"
    resource_group_name  = azurerm_resource_group.main.name
    virtual_network_name = azurerm_virtual_network.main.name
    address_prefixes       = ["10.2.0.0/24"]
}

# Create public IPs
resource "azurerm_public_ip" "main" {
    name                         = "${var.RG_NAME}-PublicIP"
    location                     = var.LOCATION
    resource_group_name          = azurerm_resource_group.main.name
    allocation_method            = "Dynamic"

    tags = {
        environment = var.environment
    }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "main" {
    name                = "${var.RG_NAME}-nsg"
    location            = var.LOCATION
    resource_group_name = azurerm_resource_group.main.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = var.environment
    }
}

# Create network interface
resource "azurerm_network_interface" "main" {
    name                      = "${var.RG_NAME}-NIC"
    location                  = azurerm_resource_group.main.location
    resource_group_name       = azurerm_resource_group.main.name

    ip_configuration {
        name                          = "${var.RG_NAME}-nic"
        subnet_id                     = azurerm_subnet.main.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.main.id
    }

    tags = {
        environment = var.environment
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "example" {
    network_interface_id      = azurerm_network_interface.main.id
    network_security_group_id = azurerm_network_security_group.main.id
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = azurerm_resource_group.main.name
    }

    byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "main" {
    name                        = "diag${random_id.randomId.hex}"
    resource_group_name         = azurerm_resource_group.main.name
    location                    = var.LOCATION
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        environment = var.environment
    }
}

# Create (and display) an SSH key
resource "tls_private_key" "example_ssh" {
  algorithm = "RSA"
  rsa_bits = 4096
}
output "tls_private_key" { 
    value = tls_private_key.example_ssh.private_key_pem 
    sensitive = true
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "vm1" {
    name                  = var.VM_NAME
    location              = var.LOCATION
    resource_group_name   = azurerm_resource_group.main.name
    network_interface_ids = [azurerm_network_interface.main.id]
    size                  = "Standard_DS1_v2"

    os_disk {
        name              = "myOsDisk"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = var.VM_NAME
    admin_username = var.VM_ADMIN
    admin_password = var.VM_PASSWORD
    disable_password_authentication = false

   # admin_ssh_key {
   #     username       = "var.VM_Name"
    #    public_key     = tls_private_key.example_ssh.public_key_openssh
    #}

    #boot_diagnostics {
     #   storage_account_uri = azurerm_storage_account.havidastorageacc.primary_blob_endpoint
    #}

    tags = {
        environment = var.environment
    }
}
