variable "RG_NAME" {
  default = "Gitlab-deault-IaC"
}

variable "VM_NAME" {
  default = "Gitlab-TestVM"
}

variable "VM_ADMIN" {
  default = "azure-admin"
}

variable "LOCATION" {
  default = "West Europe"
}

variable "DEFAULT_SSHKEY" {
  default = "yourpublicsshkey"
}

variable "VM_PASSWORD" {
  description = "Please provide Password"
}

variable "environment" {
  description = "Please provide Environment Details"
  default = "Gitlab-IaC-Practice"
}

variable "Stgacc_bcktf" {
  description = "this variale is used to storing stg acc info forterraform state file"
}

variable "stg_container_bcktf" {
  description = "this variale is used to storing contain info for storing terraform state file"
}
