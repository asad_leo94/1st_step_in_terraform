terraform {
  backend "azurerm" {
    resource_group_name  = "Gitlab_admin"
    storage_account_name = "gitiacstg123"
    container_name       = "tfstate-git-vm"
    key                  = "dev.terraform.tfstate"
  }
}
